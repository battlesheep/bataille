package boardGame;

import java.io.Serializable;
import java.util.ArrayList;

import exceptions.FieldOutOfBoardException;

/**
 * The class is used to create a board.
 * 
 * @author Victor MOULARD
 * @author Louis OLIVE
 * @author Nicolas WEINACHTER
 *
 */

public class Board implements Serializable {
	private static final long serialVersionUID = -1824712390L;
	private Field[][] fields;
	private int size;

	/**
	 * Creates a board based on the given size.
	 *
	 * @param size
	 *            Board size
	 */
	public Board(int size) {
		this.size = size;
		this.fields = new Field[size][size];
		for (int row = 0; row < fields.length; row++) {
			for (int column = 0; column < fields[row].length; column++) {
				this.fields[row][column] = new Field(column, row);
			}
		}
	}

	/**
	 * Returns all fields of the board.
	 *
	 * @return all fields
	 */
	public Field[][] getFields() {
		return fields;
	}

	/**
	 * Returns a specific field from the board.
	 *
	 * @param x
	 * @param y
	 * @return the field at position x / y
	 * @throws FieldOutOfBoardException
	 *             If the coordinates are not within the field are located.
	 */
	public Field getField(int x, int y) throws FieldOutOfBoardException {
		if (!this.containsFieldAtPosition(x, y)) {
			throw new FieldOutOfBoardException(new Field(x, y));
		}
		return this.fields[y][x];
	}

	/**
	 * Check to see if the board has a field with the given coordinates .
	 *
	 * @param x
	 *            coordinate x
	 * @param y
	 *            coordinate y
	 * @return true if the field is included, false if not
	 */
	public boolean containsFieldAtPosition(int x, int y) {
		return (x < this.size) && (y < this.size) && (x >= 0) && (y >= 0);
	}

	public int getSize() {
		return size;
	}

	/**
	 * Returns all field states of a board depending on whether it is your own
	 * Board or the board of an opponent. It will only be field conditions Which
	 * the player may know.
	 *
	 * @param isOwnBoard
	 *            Indicates whether it is your own board
	 * @return
	 */
	public FieldState[][] getFieldStates(boolean isOwnBoard) {
		FieldState[][] fieldStates = new FieldState[size][size];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				FieldState state = fields[i][j].getState();
				if ((state == FieldState.HAS_SHIP || state == FieldState.IS_EMPTY) && (!isOwnBoard)) {
					fieldStates[i][j] = null;
				} else {
					fieldStates[i][j] = state;
				}
			}
		}
		return fieldStates;
	}

	public ArrayList<Field> getFieldsOfShip(Field sourceField) {
		ArrayList<Field> result = new ArrayList<Field>();
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (fields[i][j].hasShip()) {
					if (fields[i][j].getShip() == sourceField.getShip()) {
						result.add(fields[i][j]);
					}
				}
			}
		}
		return result;
	}
}
