package exceptions;

import ship.Ship;

/**
 * exception class to handle if a ship is out of the board
 * 
 * @author Victor MOULARD
 * @author Louis OLIVE
 * @author Nicolas WEINACHTER
 */

public class ShipOutOfBoardException extends Exception {
	private Ship ship;

	public ShipOutOfBoardException(Ship ship) {
		super("Ship " + ship + " is not within the allowed board area!");
		this.ship = ship;
	}
	
	public Ship getShip() {
		return ship;
	}
}
