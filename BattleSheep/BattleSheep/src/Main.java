import java.util.ArrayList;
import java.util.Scanner;

import boardGame.*;
import exceptions.*;
import player.*;
import ship.Ship;

/**
 * @author Victor MOULARD
 * @author Louis OLIVE
 * @author Nicolas WEINACHTER
 *
 *         //\\ START HERE //\\
 */
public class Main {
	public static void main(String[] args) {
		new ConsoleGame();
	}
}

class ConsoleGame {
	private static final String SAVEGAME_FILENAME = "savegame.sav";
	private Scanner input;
	private Game game;

	public ConsoleGame() {
		input = new Scanner(System.in);
		createGame();
		gameLoop();
		prGameStasis();
		input.close();
	}

	private void createGame() {
		this.game = null;
		do {
		
			System.out.println("(1) New game");
			System.out.println("(2) Resume last saved game");
			System.out.println("(3) Help");
			System.out.println("(4) Quit");
			int choice = readIntegerWithMinMax(1, 4);

			switch (choice) {
			case 1:
				createGameManually();
				break;
			case 2:
				tryToLoadGame();
				if (game != null)
					this.makePlayerTurn();
			case 3:
				help();
				break;
			case 4:
				System.exit(0);
			}
		} while (this.game == null);
	}

	private void createGameManually() {
		// Create a game with manual settings
		Settings settings = createSettings();
		// Settings settings = null;
		//1settings = new Settings(0, 10, 1, 1, 1, 1);
		if (settings != null) {
			game = new Game();
			game.initialize(settings);
			placeShipsManually();
		}
	}

//	private void createGameAuto() {
//		Settings settings = null;
//		settings = new Settings(2, 15, 3, 2, 1, 4);
//	}

	private void tryToLoadGame() {
		// Saved game
		try {
			game = new Game();
			game.load(SAVEGAME_FILENAME);
		} catch (Exception e) {
			System.out.println("Could not load game");
			game = null;
		}
	}

	// TEST FONCTION TO CREATE THE BOARD MANUALY//
	private Settings createSettings() {
		// create settings manually
		int maxPlayers = Settings.MAX_PLAYERS;
		int players = 0;

		System.out.println("Settings");
		do {
			System.out.print("Number of player (0-" + maxPlayers + "): ");
			players = readIntegerWithMinMax(0, maxPlayers);
		} while (players < 1);

		System.out.println("Compose your fleet");
		System.out.print("Destroyer (0 - 3):  ");
		int destroyer = readIntegerWithMinMax(0, 3);
		System.out.print("Croiseur (0 - 2)");
		int croiseur = readIntegerWithMinMax(0, 2);
		System.out.print("Cuirasse (0 - 1) ");
		int cuirasse = readIntegerWithMinMax(0, 1);
		System.out.print("Sous Marin (0 - 4)");
		int sousMarin = readIntegerWithMinMax(0, 4);
		Settings settings = new Settings(players, 15, destroyer, croiseur, cuirasse, sousMarin);
		return settings;
	}

	private void setPlayerNames(Player[] players) {
		System.out.println("Name? :");
		for (Player player : players) {
			System.out.print("Name for" + player + " : ");
			player.setName(input.nextLine());
		}
	}

/*	private void placeShips() {
		// Set ships
		do {
			Player currentPlayer = game.getCurrentPlayer();
			// If not, read in the coordinates
			// placeShipsManually();
			if (currentPlayer instanceof Human) {
				System.out.println(currentPlayer + " Ships...");
				try {
					((Human) currentPlayer).placeShips();
				} catch (ShipAlreadyPlacedException e) {
					e.printStackTrace();
				} catch (FieldOutOfBoardException e) {
					e.printStackTrace();
				} catch (ShipOutOfBoardException e) {
					e.printStackTrace();
				}

				System.out.println();
				System.out.println("Board of " + currentPlayer);
				System.out.println();
				try {
					prBoard(game.getCurrentPlayer().getFieldStates(true));
				} catch (FieldOutOfBoardException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				game.nextPlayer();
			} else {
				placeShipsManually();
			}

		} while (!game.isReady());
	}*/

	private void placeShipsManually() {
		Player currentPlayer = game.getCurrentPlayer();
		boolean isItPossibleToPlaceShip;
		do {
			System.out.println(currentPlayer + " Sets ship: " + currentPlayer.getCurrentShip());
			// as long as ship coordinates read until no exception occurs
			int[] coordinates = readCoordinates(game.getBoardSize());
			Orientation orientation = readOrientation();
			isItPossibleToPlaceShip = false;
			try {
				isItPossibleToPlaceShip = game.getCurrentPlayer().placeShip(coordinates[1], coordinates[0],
						orientation);
				if (!isItPossibleToPlaceShip) {
					System.out.println("Field is already occupied or can not be used");
					System.out.println("Board reset? (Y / N)");
					boolean reset = input.next().toUpperCase().charAt(0) == 'Y' ? true : false;
					if (reset) {
						currentPlayer.resetBoard();
						System.out.println("Board reset.");
					}

				}
			} catch (ShipAlreadyPlacedException e) {
				System.out.println("Ship already placed!");
			} catch (FieldOutOfBoardException e) {
				System.out.println("Field is not on the board");
			} catch (ShipOutOfBoardException e) {
				System.out.println("Ship is not on the board!");
			}
		} while (!isItPossibleToPlaceShip);

		try {
			System.out.println();
			prBoard(currentPlayer.getFieldStates(true));
		} catch (FieldOutOfBoardException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (!currentPlayer.hasPlacedAllShips()) {
			currentPlayer.nextShip();
		} else {
			game.nextPlayer();
		}

	}

	private void gameLoop() {
		do {
			Player currentPlayer = game.getCurrentPlayer();
			// Nombre de tour
			if (game.getTurnNumber() % game.getPlayers().length == 0) {
				System.out.println();
				System.out.println("Tour " + game.getRoundNumber());
				System.out.println("-----------------------------------------------------------------------");
				System.out.println();
			}
			// Players skip when he is dead
			if (currentPlayer.hasLost()) {
				System.out.println(currentPlayer + "Is defeated and can not shoot.");
			} else if (currentPlayer.areAllShipsReloading()) {
				System.out.println(currentPlayer + "Can not shoot, since all ships are reloading.");
			} else {
				makePlayerTurn();
			}

			game.nextPlayer();
		} while (!game.isGameover());
	}

	private void makePlayerTurn() {
		Player currentPlayer = game.getCurrentPlayer();
		System.out.println();
		System.out.println(currentPlayer + "'s turn");
		System.out.println();
		System.out.println("What do you want to do?");
		System.out.println("(1) Perform ennemy attack");
		System.out.println("(X) Move a ship");
		System.out.println("(2) save game");
		System.out.println("(3) Exit");
		boolean hasAttacked = false;
		do {
			int choice = readIntegerWithMinMax(1, 3);
			switch (choice) {
			case 1:
				attackManually();
				hasAttacked = true;
				break;
			case 2:
				saveGame();
				break;
			case 3:
				System.exit(0);
			}
		} while (!hasAttacked);
	}

	private void attackManually() {
		Player enemy;

		// Ship selection
		System.out.println("Which ship should be shot?");
		selectShip();

		// Player selection
		System.out.println("which player?");
		enemy = selectEnemy(game.getEnemiesOfCurrentPlayer());
		try {
			prBoard(enemy.getFieldStates(false));
		} catch (FieldOutOfBoardException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		boolean isShotPossible = false;
		do {
			// Read the coordinates until the shot was successfully executed
			int[] coordinates = readCoordinates(game.getBoardSize());
			try {
				isShotPossible = game.makeTurn(enemy, coordinates[1], coordinates[0], readOrientation());
				if (!isShotPossible)
					System.out.println("Field has already been shot!");
				else
					prBoards(game.getCurrentPlayer().getFieldStates(true), enemy.getFieldStates(false));
			} catch (Exception e) {
				e.printStackTrace();
			}
		} while (!isShotPossible);

	}

	private void selectShip() {
		Player currentPlayer = game.getCurrentPlayer();
		ArrayList<Ship> availableShips = currentPlayer.getAvailableShips(false);
		Ship selectedShip;
		do {
			// Enter repeat until ship was selected, which can shoot
			for (Ship s : availableShips) {
				System.out.println("(" + availableShips.indexOf(s) + ") " + s.getType() + "(reload:"
						+ s.getCurrentReloadTime() + "," + " health:" + s.getSize() + ")");
			}
			selectedShip = availableShips.get(readIntegerWithMinMax(0, availableShips.size() - 1));
			try {
				currentPlayer.selectShip(selectedShip);
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (currentPlayer.getCurrentShip().isReloading()) {
				System.out.println("Ship is loading");
			}
		} while (currentPlayer.getCurrentShip().isReloading());
	}

	private Player selectEnemy(ArrayList<Player> enemies) {
		// Show attackable opponents
		for (int i = 0; i < enemies.size(); i++) {
			System.out.println("(" + i + ")" + enemies.get(i));
		}
		return enemies.get(readIntegerWithMinMax(0, enemies.size() - 1));
	}

	private void saveGame() {
		try {
			game.save(SAVEGAME_FILENAME);
		} catch (Exception e) {
			System.err.print("The game could not be saved.");
			e.printStackTrace();
		}
		System.out.println("\n\n Game saved!");
	}

	/**
	 * HEPL tab of the menu
	 */
	private void help() {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		sb.append("Legends: \n");
		sb.append("O = hit ship\n");
		sb.append("X = next to it\n");
		sb.append("~ = empty\n");
		sb.append("U = unknown\n");
		sb.append("! = Destroyed ship \n\n\n");
		sb.append("Commands: \n");
		sb.append("Just enter the coordinates numbers in the console or the GUI\n\n\n");
		sb.append("Follow the instruction and have fun!");
		System.out.println(sb.toString());
	}

	private void prBoards(FieldState[][] ownBoard, FieldState[][] enemyBoard) {

		System.out.println();
		System.out.println("My board");
		System.out.println();
		prBoard(ownBoard);
		System.out.println();
		System.out.println("Opponent board");
		System.out.println();
		prBoard(enemyBoard);
		System.out.println(
				"O = hit ship\nX = next to it\n + = own ship \n ~ = empty\n U = unknown\n ! = Destroyed ship \n");
	}

	private void prFieldColumnNumbers(int length) {
		// TODO Auto-generated method stub
		String result = "\t";
		for (int i = 1; i <= length; i++) {
			String number = "" + i + "\t";
			if (i < 10) {
				number = "0" + i + "\t";
			}
			result += number;
		}
		System.out.println(result);
	}

	/**
	 * Print game stats
	 */
	private void prGameStasis() {
		// TODO Auto-generated method stub
		System.out.println();
		System.out.println("Game Over");
		System.out.println((int) Math.floor(game.getTurnNumber() / game.getPlayers().length) + " Round");
		for (Player player : game.getPlayers()) {
			if (player.hasLost()) {
				System.out.println(player + " Is defeated.");
			}
		}
		System.out.println(game.getWinner() + " win!");
	}

	/**
	 * Print game state
	 */
	private void prState(FieldState fieldState) {
		// TODO Auto-generated method stub
		String s = "";
		if (fieldState == null) {
			s = "?";
		} else {
			switch (fieldState) {
			case DESTROYED:
				s = "!";
				break;
			case HIT:
				s = "O";
				break;
			case MISSED:
				s = "X";
				break;
			case HAS_SHIP:
				s = "+";
				break;
			case IS_EMPTY:
				s = "~";
				break;
			default:
				break;
			}
		}
		System.out.print(s);
	}

	/**
	 * Print board
	 */
	private void prBoard(FieldState[][] fieldStates) {
		// TODO Auto-generated method stub

		this.prFieldColumnNumbers(fieldStates[0].length);
		System.out.println();
		for (int row = 0; row < fieldStates.length; row++) {
			String number = row + 1 < 10 ? "0" + (row + 1) : "" + (row + 1);
			System.out.print(number + "\t");
			for (int column = 0; column < fieldStates[row].length; column++) {
				FieldState field = fieldStates[row][column];
				prState(field);
				System.out.print("\t");
			}
			System.out.println();
		}
		System.out.println();
	}

	private int readInteger() {
		// TODO Auto-generated method stub
		while (!input.hasNextInt()) {
			System.out.println("Enter a number!");
			input.next();
		}
		return input.nextInt();
	}

	private int readIntegerWithMinMax(int min, int max) {
		// TODO Auto-generated method stub
		int i;
		boolean isValid = false;
		do {
			i = readInteger();
			isValid = (i >= min) && (i <= max);
			if (!isValid)
				System.out.println("Enter a number between min " + min + " and " + max + "!");
		} while (!isValid);
		return i;
	}

	/**
	 * Read coordinates
	 * 
	 * @param boardSize
	 *            Size of the board
	 * @return Coordinates in a tab
	 */
	private int[] readCoordinates(int boardSize) {
		// Read in the line
		System.out.print("Line (1-" + boardSize + "): ");
		int row = readIntegerWithMinMax(1, boardSize) - 1;

		// Read in the column
		System.out.print("Column (1-" + boardSize + "): ");
		int column = readIntegerWithMinMax(1, boardSize) - 1;
		return new int[] { row, column };
	}

	/**
	 * Read orientation
	 * 
	 * @return orientation
	 */
	private Orientation readOrientation() {
		// Read the alignment
		System.out.print("Orientation (H/V): ");
		Orientation orientation = input.next().toUpperCase().charAt(0) == 'V' ? Orientation.VERTICAL
				: Orientation.HORIZONTAL;
		return orientation;
	}
}
