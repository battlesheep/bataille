package player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import boardGame.Field;
import boardGame.Orientation;
import exceptions.*;
import ship.Ship;

/**
 * This class define a human's player
 * 
 * @author Victor MOULARD
 * @author Louis OLIVE
 * @author Nicolas WEINACHTER
 */

public class Human extends Player {
	private static final long serialVersionUID = 5389576523L;
	private static final int MAX_TRIES_TO_PLACE_SHIP = 10;

	public Human(int boardSize, int destroyer, int croiseur, int cuirasse, int sousMarin) {
		super(boardSize, destroyer, croiseur, cuirasse, sousMarin);
		this.name = "Player";
		this.type = PlayerType.HUMAN;
	}

	public Human(int boardSize, HashMap<Ship, ArrayList<Field>> shipMap) {
		super(boardSize, shipMap);
		this.type = PlayerType.HUMAN;
		this.name = "Player";
	}

	/***
	 * The method is used to automatically place and set all ships.
	 * 
	 * @throws ShipAlreadyPlacedException*
	 *             If a ship has already been placed
	 * @throws FieldOutOfBoardException*
	 *             If a field is not inside the board
	 * @throws ShipOutOfBoardException*
	 *             If a ship is not completely in the field
	 */

	public void placeShips() throws ShipAlreadyPlacedException, FieldOutOfBoardException, ShipOutOfBoardException {
		boolean isItPossibleToPlaceShip;
		int i = 0;
		do {
			int counter = 0;
			do {
				currentShip = ships[i];
				isItPossibleToPlaceShip = false;
				Target target = getRandomShipPlacementTarget();
				counter++;
				isItPossibleToPlaceShip = placeShip(target.getX(), target.getY(), target.getOrientation());
			} while ((counter <= MAX_TRIES_TO_PLACE_SHIP) && (!isItPossibleToPlaceShip));

			if (counter >= MAX_TRIES_TO_PLACE_SHIP) {
				resetBoard();
				counter = 0;
				i = 0;
			} else {
				i++;
				nextShip();
			}
		} while (i < ships.length);
	}

	/**
	 * Specifies a random location and orientation to place one Ship back.
	 *
	 * @return placement destination for a ship
	 */
	private Target getRandomShipPlacementTarget() {
		// Generate random position
		Orientation orientation;
		orientation = (createRandomNumber(0, 1) == 0) ? Orientation.HORIZONTAL : Orientation.VERTICAL;
		int xMax;
		int yMax;
		if (orientation == Orientation.HORIZONTAL) {
			xMax = this.board.getSize() - this.getCurrentShip().getSize();
			yMax = this.board.getSize() - 1;
		} else {
			xMax = this.board.getSize() - 1;
			yMax = this.board.getSize() - this.getCurrentShip().getSize();

		}
		int xPos = createRandomNumber(0, xMax);
		int yPos = createRandomNumber(0, yMax);
		return new Target(xPos, yPos, orientation);
	}

	/**
	 * Returns a random number
	 *
	 * @param min
	 *            The minimum size of the number
	 * @param max
	 *            Maximum size of the number
	 * @return a random number
	 */
	private int createRandomNumber(int min, int max) {
		Random random = new Random();
		return random.nextInt(max - min + 1) + min;
	}
}
