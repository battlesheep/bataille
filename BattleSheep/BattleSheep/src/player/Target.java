package player;

import boardGame.Orientation;

/**
 * The Target class includes the position and orientation of a shot.
 * 
 * @author Victor MOULARD
 * @author Louis OLIVE
 * @author Nicolas WEINACHTER
 */

public class Target {
	private int x;
	private int y;
	private Orientation orientation;

	public Target(int x, int y, Orientation orientation) {
		this.x = x;
		this.y = y;
		this.orientation = orientation;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public Orientation getOrientation() {
		return orientation;
	}

}
