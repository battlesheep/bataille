package player;

/**
 * This enumeration was originally created in order to add other player'stype
 * such as IAplayer
 * 
 * @author Victor MOULARD
 * @author Louis OLIVE
 * @author Nicolas WEINACHTER
 */

public enum PlayerType {
	HUMAN
}
