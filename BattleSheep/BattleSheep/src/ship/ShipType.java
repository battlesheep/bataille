package ship;

/**
 * The enumeration of all ships' type.
 * 
 * @author Victor MOULARD
 * @author Louis OLIVE
 * @author Nicolas WEINACHTER
 *
 */
public enum ShipType {
	CUIRASSE, CROISEUR, DESTROYER, SOUSMARIN
}
