package ship;

import java.io.Serializable;

import boardGame.Settings;

/**
 * The abstract class Ship serves as the basis for special ship classes.
 * 
 * @author Victor MOULARD
 * @author Louis OLIVE
 * @author Nicolas WEINACHTER
 *
 */
public abstract class Ship implements Serializable {
	private static final long serialVersionUID = 98213987433L;

	protected ShipType type;
	protected int size;

	protected int shootingRange;
	protected int maxReloadTime;
	protected int currentReloadTime;

	protected boolean isPlaced;

	/**
	 * Reset every ship
	 */

	public void reset() {
		this.isPlaced = false;
		this.currentReloadTime = 0;

		switch (type) {
		case DESTROYER:
			this.size = Settings.DESTROYER_SIZE;
			break;
		case CROISEUR:
			this.size = Settings.CROISEUR_SIZE;
			break;
		case CUIRASSE:
			this.size = Settings.CUIRASSE_SIZE;
			break;
		case SOUSMARIN:
			this.size = Settings.SOUSMARIN_SIZE;
			break;
		}
	}

	/**
	 * * Sets the reload time to the maximum value before shooting.
	 */
	public void shoot() {
		currentReloadTime = maxReloadTime + 1;
	}

	/**
	 * downshift the reload time
	 */
	public void decreaseCurrentReloadTime() {
		if (currentReloadTime > 0) {
			currentReloadTime--;
		}
	}

	/**
	 * Indicates whether the ship is just loading.
	 * 
	 * @return true if the ship is loading, false if not.
	 */
	public boolean isReloading() {
		return currentReloadTime > 0;
	}

	public void place() {
		this.isPlaced = true;
	}

	public boolean isPlaced() {
		return isPlaced;
	}

	public void setPlaced(boolean isPlaced) {
		this.isPlaced = isPlaced;
	}

	public boolean isDestroyed() {
		return size <= 0;
	}

	public void decreaseSize() {
		if (size > 0) {
			size--;
		}
	}

	public int getSize() {
		return size;
	}

	public int getShootingRange() {
		return shootingRange;
	}

	public int getMaxReloadTime() {
		return maxReloadTime;
	}

	public int getCurrentReloadTime() {
		return currentReloadTime;
	}

	public ShipType getType() {
		return type;
	}

	public String toString() {
		return type.toString();
	}

}
