package ship;

import boardGame.Settings;

/**
 * Cuirasse class
 * 
 * @author Victor MOULARD
 * @author Louis OLIVE
 * @author Nicolas WEINACHTER
 */
public class Cuirasse extends Ship {
	private static final long serialVersionUID = -10514250658L;//-6210514250671045537L

	public Cuirasse() {
		this.size = Settings.CROISEUR_SIZE;
		this.shootingRange = 15;
		this.maxReloadTime = 9;
		this.type = ShipType.CUIRASSE;
	}

}
