package ship;

import boardGame.Settings;

/**
 * Destroyer Class
 * 
 * @author Victor MOULARD
 * @author Louis OLIVE
 * @author Nicolas WEINACHTER
 */
public class Destroyer extends Ship {
	private static final long serialVersionUID = -19681463452L; //1668124704540481822L

	public Destroyer() {
		this.size = Settings.DESTROYER_SIZE;
		this.shootingRange = 15;
		this.maxReloadTime = 1;
		this.type = ShipType.DESTROYER;
	}
}
