package ship;

import boardGame.Settings;


/**
 * Submarine class
 * 
 * @author Victor MOULARD
 * @author Louis OLIVE
 * @author Nicolas WEINACHTER
 */
public class SousMarin extends Ship {
	private static final long serialVersionUID = -54879626113L;//-4487962641047449170L

	public SousMarin() {
		this.size = Settings.SOUSMARIN_SIZE;
		this.shootingRange = 15;
		this.maxReloadTime = 1;
		this.type = ShipType.SOUSMARIN;
	}
}
