package ship;

import boardGame.Settings;

/**
 * Croiseur class
 * 
 * @author Victor MOULARD
 * @author Louis OLIVE
 * @author Nicolas WEINACHTER
 */
public class Croiseur extends Ship {
	private static final long serialVersionUID = 59010643126L;

	public Croiseur() {
		this.size = Settings.CROISEUR_SIZE;
		this.shootingRange = 15;
		this.maxReloadTime = 4;
		this.type = ShipType.CROISEUR;
	}

}
