package exceptions;

import boardGame.Field;

/**
 * exception class to handle every action out of the board
 * 
 * @author Victor MOULARD
 * @author Louis OLIVE
 * @author Nicolas WEINACHTER
 */
public class FieldOutOfBoardException extends Exception {
	private Field field;

	public FieldOutOfBoardException(Field field) {
		super("Field at position x=" + field.getXPos() + ", y=" + field.getYPos() + " is not on the Board!");
		this.field = field;
	}

	public Field getField() {
		return field;
	}
}
