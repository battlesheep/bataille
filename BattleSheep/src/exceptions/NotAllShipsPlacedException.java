package exceptions;

import player.Player;

/**
 * exception class to handle the case where a player has not placed all his
 * ships this is apliable only on the manual placing of the ships
 * 
 * @author Victor MOULARD
 * @author Louis OLIVE
 * @author Nicolas WEINACHTER
 */
public class NotAllShipsPlacedException extends Exception {
	private Player player;

	public NotAllShipsPlacedException(Player player) {
		super(player + " has not placed (all) his ships!");
		this.player = player;
	}

	public Player getPlayer() {
		return player;
	}
}
