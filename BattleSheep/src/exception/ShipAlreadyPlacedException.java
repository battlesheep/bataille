package exception;

import ship.Ship;

/**
 * exception class to avoid placing the same ship twice
 * 
 * @author Victor MOULARD
 * @author Louis OLIVE
 * @author Nicolas WEINACHTER
 */


public class ShipAlreadyPlacedException extends Exception {
	private Ship ship;
	
	public ShipAlreadyPlacedException(Ship ship) {
		super("Ship " + ship + " is already placed!");
		this.ship = ship;
	}
	
	public Ship getShip() {
		return ship;
	}
}
