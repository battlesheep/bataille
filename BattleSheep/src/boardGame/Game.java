package boardGame;

import ship.Ship;
import player.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.io.*;

import exceptions.FieldOutOfBoardException;

/**
 * @author Victor MOULARD
 * @author Louis OLIVE
 * @author Nicolas WEINACHTER
 */
public class Game {
	private static final long serialVersionUID = -8897834234L;

	private Player[] players;
	private Player currentPlayer;
	private Player winner;
	private boolean hasCurrentPlayerMadeTurn;

	private int turnNumber;
	private int roundNumber;
	private int boardSize;

	private Field[] fieldLastTurn;
	private Settings settings;

	/**
	 * Initializes the required objects for the game using the Passed settings.
	 */
	public void initialize(Settings settings) {
		this.settings = settings;
		createPlayers(settings);

		// Set player numbers
		for (int i = 0; i < players.length; i++) {
			players[i].setName(players[i].getName() + (i + 1));
		}

		boardSize = settings.getBoardSize();
		turnNumber = 0;
		roundNumber = 0;
		currentPlayer = players[0];
	}

	/**
	 * Creates the players based on the given settings.
	 */
	private void createPlayers(Settings settings) {
		int numberOfPlayers = settings.getPlayers();
		players = new Player[numberOfPlayers];
		for (int i = 0; i < numberOfPlayers; i++) {

			players[i] = new Human(settings.getBoardSize(), settings.getDestroyer(), settings.getCroiseur(),
					settings.getCuirasse(), settings.getSousMarin());

		}
	}

	/**
	 * The method is used to run a train. She gets the And the position and
	 * alignment of the shot. Returns false if a shot is not possible. This is
	 * The case when the initial field has already been shot.
	 *
	 * @param enemy
	 *            The opponent to attack
	 * @param xPos
	 *            Start-X position of the shot
	 * @param yPos
	 *            Start-Y position of the shot
	 * @param orientation
	 *            Alignment of the shot
	 * @return true if the shot was possible, false if not
	 */

	public boolean makeTurn(Player enemy, int xPos, int yPos, Orientation orientation) throws FieldOutOfBoardException {
		Field[] markedFields = new Field[currentPlayer.getCurrentShip().getShootingRange()];
		int xDirection = orientation == Orientation.HORIZONTAL ? 1 : 0;
		int yDirection = orientation == Orientation.VERTICAL ? 1 : 0;
		int x;
		int y;
		for (int i = 0; i < currentPlayer.getCurrentShip().getShootingRange(); i++) {
			x = xPos + i * xDirection;
			y = yPos + i * yDirection;
			boolean isShotPossible = enemy.markBoard(x, y);
			if (i == 0) {
				if (!isShotPossible) {
					// erstes Feld belegt, Schuss nicht möglich
					return false;
				}
			}
			if (isShotPossible) {
				markedFields[i] = enemy.getBoard().getField(x, y);
			}
		}
		boolean isDestroyed = false;
		ArrayList<Field> missedFields = new ArrayList<Field>();
		Field source = null;
		for (int i = 0; i < markedFields.length; i++) {
			if (markedFields[i] == null) {
				continue;
			}
			if (markedFields[i].hasShip()) {
				if (markedFields[i].getShip().isDestroyed()) {
					source = markedFields[i];
					isDestroyed = true;
				}
			} else {
				missedFields.add(markedFields[i]);
			}
		}
		if (isDestroyed) {
			ArrayList<Field> shipFields = enemy.getBoard().getFieldsOfShip(source);
			shipFields.addAll(missedFields);
			markedFields = shipFields.toArray(markedFields);
		}
		currentPlayer.getCurrentShip().shoot();
		hasCurrentPlayerMadeTurn = true;
		this.fieldLastTurn = markedFields;
		return true;
	}

	/**
	 * The method is used to save a game.
	 *
	 * @throws Exception
	 *             If the game could not be saved
	 */

	public void save(String destinationPath) throws Exception {
		FileOutputStream saveFile = null;
		ObjectOutputStream save = null;
		try {
			saveFile = new FileOutputStream(destinationPath);
			save = new ObjectOutputStream(saveFile);
			save.writeObject(this);
			save.close();
		} catch (Exception ex) {
			throw ex;
		} finally {
			closeQuietly(saveFile);
			closeQuietly(save);
		}
	}

	/**
	 * The method is used to load a game.
	 *
	 * @throws Exception
	 *             If the game could not be loaded.
	 */

	public void load(String destinationPath) throws Exception {
		FileInputStream saveFile = null;
		ObjectInputStream save = null;
		try {
			saveFile = new FileInputStream(destinationPath);
			save = new ObjectInputStream(saveFile);
			Game game = (Game) save.readObject();
			players = game.players;
			currentPlayer = game.currentPlayer;
			winner = game.winner;
			turnNumber = game.turnNumber;
			boardSize = game.boardSize;
			hasCurrentPlayerMadeTurn = game.hasCurrentPlayerMadeTurn;
			save.close();
		} catch (Exception ex1) {
			throw ex1;
		} finally {
			closeQuietly(saveFile);
			closeQuietly(save);
		}
	}

	/**
	 * Close InputStream
	 */
	private void closeQuietly(InputStream stream) {
		if (stream != null) {
			try {
				stream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Close OutputStream
	 */
	private void closeQuietly(OutputStream stream) {
		if (stream != null) {
			try {
				stream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * The method is used to set the current player to the next Player. In
	 * addition, the properties turnNumber and roundNumber , As well as the
	 * reloadTime of the ships down.
	 */

	public void nextPlayer() {
		turnNumber++;
		decreaseCurrentReloadTimeOfShips(currentPlayer);
		int currentPlayerIndex = Arrays.asList(players).indexOf(currentPlayer);
		// if last player in the array, then set index to 0 again, else count up
		currentPlayerIndex = (currentPlayerIndex >= players.length - 1) ? currentPlayerIndex = 0
				: currentPlayerIndex + 1;
		if (currentPlayerIndex == 0) {
			roundNumber++;
		}
		currentPlayer = players[currentPlayerIndex];
		hasCurrentPlayerMadeTurn = false;
	}

	/**
	 * Decreases the reloadTime of all ships of the passed player.
	 */
	private void decreaseCurrentReloadTimeOfShips(Player player) {
		Ship[] ships = player.getShips();
		for (Ship ship : ships) {
			ship.decreaseCurrentReloadTime();
		}
	}

	/**
	 * Provides a list of players that the current player can attack. Players
	 * who have lost or are the current player themselves will be Filtered.
	 *
	 * @return a list of attackable players
	 */

	public ArrayList<Player> getEnemiesOfCurrentPlayer() {
		// return attackable opponents to the currentPlayer
		ArrayList<Player> enemies = new ArrayList<Player>();
		for (int i = 0; i < players.length; i++) {
			if (!players[i].hasLost()) {
				if (!currentPlayer.equals(players[i])) {
					enemies.add(players[i]);
				}
			}
		}
		return enemies;
	}

	/**
	 * Provides a player with specific names.
	 *
	 * @param name
	 *            Game name
	 * @return the player with the given name
	 */
	public Player getPlayerByName(String name) {
		for (Player player : players) {
			if (player.getName().equals(name)) {
				return player;
			}
		}
		return null;
	}

	public Settings getSettings() {
		return settings;
	}

	public void setSettings(Settings settings) {
		this.settings = settings;
	}

	/**
	 * Returns true if all players have set their ships. The method Is used to
	 * check if the game is ready to start.
	 *
	 * @return true if all players have set their ships, false if Not
	 */
	public boolean isReady() {
		// checks if all ships are set
		for (Player player : players)
			if (!player.hasPlacedAllShips()) {
				return false;
			}
		return true;
	}

	/**
	 * Verifies if only one player is left. In addition, she uses the Property
	 * winner on the player who has won if the game is over.
	 *
	 * @return true if the game is over, false if not
	 */
	
	public boolean isGameover() {
		int numberOfPlayersLeft = 0;
		Player potentialWinner = null;
		for (Player player : players) {
			if (!player.hasLost()) {
				numberOfPlayersLeft++;
				potentialWinner = player;
			}
		}
		if (numberOfPlayersLeft <= 1) {
			winner = potentialWinner;
		}
		return numberOfPlayersLeft <= 1;
	}

	public Player[] getPlayers() {
		return players;
	}

	public Player getCurrentPlayer() {
		return currentPlayer;
	}

	public void setCurrentPlayer(Player currentPlayer) {
		this.currentPlayer = currentPlayer;
	}

	public Player getWinner() {
		return winner;
	}

	public int getTurnNumber() {
		return turnNumber;
	}

	public int getRoundNumber() {
		return roundNumber;
	}

	public int getBoardSize() {
		return boardSize;
	}

	public boolean hasCurrentPlayerMadeTurn() {
		return hasCurrentPlayerMadeTurn;
	}

	public void setPlayerBoards(ArrayList<Board> playerBoards) {

		for (int i = 0; i < players.length; i++) {
			HashMap<Ship, ArrayList<Field>> shipMap = getShipMap(playerBoards.get(i));

			Player player = new Human(boardSize, shipMap);
			player.setName(players[i].getName());
			for (Ship ship : player.getShips()) {
				ship.setPlaced(true);
			}
			players[i] = player;
		}
		currentPlayer = players[0];
	}

	
	/**
	 * HashMap of the ships
	 * @param board
	 * @return
	 */
	private HashMap<Ship, ArrayList<Field>> getShipMap(Board board) {
		HashMap<Ship, ArrayList<Field>> shipMap = new HashMap<Ship, ArrayList<Field>>();
		Field[][] fields = board.getFields();
		for (int row = 0; row < board.getSize(); row++) {
			for (int column = 0; column < board.getSize(); column++) {
				if (fields[row][column].getShip() != null) {
					if (!shipMap.containsKey(fields[row][column].getShip())) {
						ArrayList<Field> shipFields = new ArrayList<Field>();
						shipFields.add(fields[row][column]);
						shipMap.put(fields[row][column].getShip(), shipFields);
					} else {
						ArrayList<Field> shipFields = shipMap.get(fields[row][column].getShip());
						shipFields.add(fields[row][column]);
						shipMap.put(fields[row][column].getShip(), shipFields);
					}
				}
			}
		}

		return shipMap;
	}

	public Field[] getMarkedFieldOfLastTurn() {
		return fieldLastTurn;
	}
}
