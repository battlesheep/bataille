package boardGame;

import java.io.Serializable;

/**
 * The class includes the settings for a game. In addition, it provides Methods
 * for validating game settings.
 * 
 * @author Victor MOULARD
 * @author Louis OLIVE
 * @author Nicolas WEINACHTER
 */


public class Settings implements Serializable {
	public static final int CUIRASSE_SIZE = 7;
	public static final int CROISEUR_SIZE = 5;
	public static final int DESTROYER_SIZE = 3;
	public static final int SOUSMARIN_SIZE = 1;

	public static final int BOARD_SIZE = 15;

	public static final int PLAYERS = 2;
	public static final int MAX_PLAYERS = 2;
	
	public static final String SAVEGAME_FILENAME = "savegame.sav";
	private static final long serialVersionUID = -656987351L;
	

	private int players;
	private int boardSize;

	private int cuirasse;
	private int croiseur;
	private int destroyer;
	private int sousmarin;

	
	/**
	 * Default constructor
	 */
	public Settings() {
		this.players = 2;
		this.boardSize = 15;

		this.cuirasse = 7;
		this.croiseur = 5;
		this.destroyer = 3;
		this.sousmarin = 1;
	}

	/**
	 * Constructor to create any settings
	 *
	 * @param players
	 *            Number of players
	 * @param smartAiPlayers
	 *            Number of cunning AI players
	 * @param dumbAiPlayers
	 *            Number of stupid AI players
	 * @param boardSize
	 *            Board size
	 * @param destroyer
	 *            Number of destroyer
	 * @param croiseur
	 *            Number of croiseur
	 * @param cuirasse
	 *            Number of cuirasse
	 * @param sousMarin
	 *            Number of submarines
	 */
	public Settings(int players, int boardSize, int destroyer, int croiseur, int cuirasse, int sousMarin) {
		this.players = players;
		this.boardSize = boardSize;
		this.destroyer = destroyer;
		this.croiseur = croiseur;
		this.cuirasse = cuirasse;
		this.sousmarin = sousMarin;
	}

	public int getPlayers() {
		return players;
	}

	public void setPlayers(int players) {
		this.players = players;
	}

	public int getBoardSize() {
		return boardSize;
	}

	public void setBoardSize(int boardSize) {
		this.boardSize = boardSize;
	}

	public int getDestroyer() {
		return destroyer;
	}

	public void setDestroyer(int destroyer) {
		this.destroyer = destroyer;
	}

	public int getCroiseur() {
		return croiseur;
	}

	public void setCroiseur(int croiseur) {
		this.croiseur = croiseur;
	}

	public int getCuirasse() {
		return cuirasse;
	}

	public void setCuirasse(int cuirasse) {
		this.cuirasse = cuirasse;
	}

	public int getSousMarin() {
		return sousmarin;
	}

	public void setSousMarin(int sousmarin) {
		this.sousmarin = sousmarin;
	}

	public boolean isNumeric(String str) {
		for (char c : str.toCharArray()) {
			if (!Character.isDigit(c)) {
				return false;
			}
		}
		return true;
	}

}
