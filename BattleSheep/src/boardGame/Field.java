package boardGame;

import java.io.Serializable;

import ship.Ship;

/**
 * The class represents a single field in a board.
 * 
 * @author Victor MOULARD
 * @author Louis OLIVE
 * @author Nicolas WEINACHTER
 *
 */

public class Field implements Serializable {
	private static final long serialVersionUID = -297868363930L;
	private int xPos;
	private int yPos;
	private Ship ship;
	private boolean isHit;

	public Field(int xPos, int yPos) {
		this.xPos = xPos;
		this.yPos = yPos;
	}

	public int getXPos() {
		return xPos;
	}

	public int getYPos() {
		return yPos;
	}

	public Ship getShip() {
		return ship;
	}

	public void setShip(Ship ship) {
		if (!hasShip()) {
			this.ship = ship;
		}
	}

	public void mark() {
		isHit = true;
	}

	public boolean hasShip() {
		return ship != null;
	}

	public boolean isHit() {
		return isHit;
	}

	/**
	 * Returns the state of a field.
	 *
	 * @return FieldState
	 */
	public FieldState getState() {
		if (this.isHit()) {
			if (this.hasShip()) {
				if (this.getShip().isDestroyed()) {
					return FieldState.DESTROYED;
				} else {
					return FieldState.HIT;
				}
			} else {
				return FieldState.MISSED;
			}
		} else {
			if (this.hasShip()) {
				return FieldState.HAS_SHIP;
			} else {
				return FieldState.IS_EMPTY;
			}
		}
	}
}
