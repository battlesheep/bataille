package boardGame;

/**
 * this enumeration describe the two differents orientations that a ship can
 * take
 * 
 * @author Victor MOULARD
 * @author Louis OLIVE
 * @author Nicolas WEINACHTER
 */
public enum Orientation {
	HORIZONTAL, VERTICAL
}
