package boardGame;

/**
 * Enumeration of the possible mention that can take a part part of the field
 * 
 * @author Victor MOULARD
 * @author Louis OLIVE
 * @author Nicolas WEINACHTER
 */
public enum FieldState {
	IS_EMPTY, HIT, MISSED, DESTROYED, HAS_SHIP;

	public static FieldState[][] defaultArrayCons(int length) {
		FieldState[][] fields = new FieldState[length][length];
		for (int row = 0; row < length; row++) {
			for (int col = 0; col < length; col++) {
				fields[row][col] = FieldState.IS_EMPTY;
			}
		}
		return fields;
	}
}
