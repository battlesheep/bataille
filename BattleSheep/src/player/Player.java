package player;

import ship.*;
import exceptions.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import boardGame.*;



/**
 * This class define how a player is built
 * 
 * @author Victor MOULARD
 * @author Louis OLIVE
 * @author Nicolas WEINACHTER
 */

public abstract class Player implements Serializable {
	private static final long serialVersionUID = -7673281384L;

	protected String name;
	protected Ship[] ships;
	protected Ship currentShip;
	protected PlayerType type;
	protected Board board;

	/**
	 * Initializes the ships and the board using the Passed parameters.
	 * 
	 * @param boardSize
	 *            Size of the board
	 * @param destroyer
	 *            Number of destroyers
	 * @param croiseur
	 *            Number of croiseur
	 * @param cuirasse
	 *            Number of cuirasse
	 * @param sousMarin
	 *            Number of sousMoarin
	 */
	public Player(int boardSize, int destroyer, int croiseur, int cuirasse, int sousMarin) {
		initShips(destroyer, croiseur, cuirasse, sousMarin);
		this.board = new Board(boardSize);
		this.currentShip = this.ships[0];
	}

	/**
	 * Player attribute definition
	 * 
	 * @param boardSize
	 * @param shipMap
	 */
	public Player(int boardSize, HashMap<Ship, ArrayList<Field>> shipMap) {
		this.board = new Board(boardSize);
		ships = new Ship[shipMap.size()];
		int counter = 0;
		for (Map.Entry<Ship, ArrayList<Field>> entry : shipMap.entrySet()) {
			Ship key = entry.getKey();
			ArrayList<Field> value = entry.getValue();
			switch (key.getType()) {

			case CUIRASSE:
				Cuirasse cuirasse = new Cuirasse();
				ships[counter] = cuirasse;
				for (int fieldIndex = 0; fieldIndex < value.size(); fieldIndex++) {
					Field field = value.get(fieldIndex);
					board.getFields()[field.getYPos()][field.getXPos()].setShip(cuirasse);
				}
				break;

			case CROISEUR:
				Croiseur croiseur = new Croiseur();
				ships[counter] = croiseur;
				for (int fieldIndex = 0; fieldIndex < value.size(); fieldIndex++) {
					Field field = value.get(fieldIndex);
					board.getFields()[field.getYPos()][field.getXPos()].setShip(croiseur);
				}
				break;

			case DESTROYER:
				Ship destroyer = new Destroyer();
				ships[counter] = destroyer;
				for (int fieldIndex = 0; fieldIndex < value.size(); fieldIndex++) {
					Field field = value.get(fieldIndex);
					board.getFields()[field.getYPos()][field.getXPos()].setShip(destroyer);
				}
				break;

			case SOUSMARIN:
				SousMarin sousMarin = new SousMarin();
				ships[counter] = sousMarin;
				for (int fieldIndex = 0; fieldIndex < value.size(); fieldIndex++) {
					Field field = value.get(fieldIndex);
					board.getFields()[field.getYPos()][field.getXPos()].setShip(sousMarin);
				}
				break;
			}
			counter++;
		}
		this.currentShip = this.ships[0];
	}

	/**
	 * This method reset the board
	 */
	public void resetBoard() {
		int size = board.getSize();
		board = new Board(size);
		for (Ship ship : ships) {
			ship.setPlaced(false);
		}
		currentShip = ships[0];
	}

	/**
	 * this method initate the ships
	 * 
	 * @param destroyer
	 *            new destroyer
	 * @param croiseur
	 *            new croiseur
	 *
	 * @param cuirasse
	 *            new cuirasse
	 * @param sousMarin
	 *            new sous marin
	 */
	private void initShips(int destroyer, int croiseur, int cuirasse, int sousMarin) {
		ships = new Ship[destroyer + croiseur + cuirasse + sousMarin];
		for (int i = 0; i < ships.length; i++) {
			if (i < destroyer) {
				ships[i] = new Destroyer();
			} else if (i < destroyer + croiseur) {
				ships[i] = new Croiseur();
			} else if (i < destroyer + croiseur + cuirasse) {
				ships[i] = new Cuirasse();
			} else {
				ships[i] = new SousMarin();
			}
		}
	}

	/**
	 * Returns all field states of the player board depending on whether it is
	 * Own board or the board of an opponent. It will only Field conditions
	 * returned, which the player may know.
	 *
	 * @param isOwnBoard
	 *            Indicates whether it is your own board
	 */
	public FieldState[][] getFieldStates(boolean isOwnBoard) throws FieldOutOfBoardException {
		int size = board.getSize();
		FieldState[][] fieldStates = new FieldState[size][size];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				FieldState state = board.getField(j, i).getState();
				if ((state == FieldState.HAS_SHIP || state == FieldState.IS_EMPTY) && (!isOwnBoard)) {
					fieldStates[i][j] = null;
				} else {
					fieldStates[i][j] = state;
				}
			}
		}
		return fieldStates;
	}

	public FieldState[][] getFieldWithStateEmpty() {
		int size = board.getSize();
		FieldState[][] fieldStates = new FieldState[size][size];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				fieldStates[i][j] = FieldState.IS_EMPTY;
			}
		}
		return fieldStates;
	}

	/**
	 * Check whether it is possible to ship a ship to a specific position . If
	 * so, the ship is placed and true , Otherwise false is returned.
	 *
	 * @param xPos
	 *            X coordinate
	 * @param yPos
	 *            Y coordinate
	 * @param orientation
	 *            Orientation
	 * @return true if the ship can be placed at this point, False if not
	 * @throws ShipAlreadyPlacedException
	 *             If the ship has already been placed
	 * @throws FieldOutOfBoardException
	 *             If the field is not in the board
	 * @throws ShipOutOfBoardException
	 *             If the ship is not (partially) in the board
	 */
	public boolean placeShip(int xPos, int yPos, Orientation orientation)
			throws ShipAlreadyPlacedException, FieldOutOfBoardException, ShipOutOfBoardException {
		if (isItPossibleToPlaceShip(xPos, yPos, orientation)) {
			placeShipOnBoard(this.currentShip, xPos, yPos, orientation);
			return true;
		}
		return false;
	}

	/**
	 * Check if it is possible to ship the ship at the delivered position .
	 *
	 * @return true if it is possible, false if not
	 */
	public boolean isItPossibleToPlaceShip(int xPos, int yPos, Orientation orientation)
			throws ShipOutOfBoardException, ShipAlreadyPlacedException, FieldOutOfBoardException {
		if (this.currentShip.isPlaced()) {
			throw new ShipAlreadyPlacedException(this.currentShip);
		}

		if (!this.board.containsFieldAtPosition(xPos, yPos)) {
			throw new FieldOutOfBoardException(new Field(xPos, yPos));
		}

		if (isShipPartiallyOutOfBoard(this.currentShip, xPos, yPos, orientation)) {
			throw new ShipOutOfBoardException(this.currentShip);
		}

		Field occupiedField = findOccupiedField(this.currentShip, xPos, yPos, orientation);
		if (occupiedField != null) {
			return false;
		}
		return true;
	}

	/**
	 * Check whether fields are already occupied at the desired location. At the
	 * same time It is considered that around the ship a field remain free got
	 * to.
	 *
	 * @return the first occupied field that was found null if there is no
	 *         Occupied field
	 */
	private Field findOccupiedField(Ship ship, int xPos, int yPos, Orientation orientation) {
		Field[][] fields = this.board.getFields();
		// Orientation Horizontal
		if (orientation == Orientation.HORIZONTAL) {

			// Check the fields already checked
			for (int y = yPos - 1; y <= yPos + 1; y++)
				for (int x = xPos - 1; x <= xPos + ship.getSize(); x++)

					if (x >= 0 && y >= 0 && x < fields.length && y < fields.length) {
						if (fields[y][x].getShip() != null) {
							return (fields[y][x]);
						}
					}
		}

		// Orientation Vertical
		if (orientation == Orientation.VERTICAL) {
			// Check the fields already checked
			for (int y = yPos - 1; y <= yPos + ship.getSize(); y++)
				for (int x = xPos - 1; x <= xPos + 1; x++)
					// x and y within the playing field
					if (x >= 0 && y >= 0 && x < fields.length && y < fields.length) {

						// Field has ship
						if (fields[y][x].getShip() != null) {
							return (fields[y][x]);
						}
					}
		}
		return null;
	}

	/**
	 * Check whether the ship is outside the board or partially
	 *
	 * @return true if the ship is outside the board, false unless
	 */
	private boolean isShipPartiallyOutOfBoard(Ship ship, int xPos, int yPos, Orientation orientation) {
		int xDirection = orientation == Orientation.HORIZONTAL ? 1 : 0;
		int yDirection = orientation == Orientation.VERTICAL ? 1 : 0;
		int x = xPos + ship.getSize() * xDirection - 1;
		int y = yPos + ship.getSize() * yDirection - 1;
		return (x >= board.getSize()) || (y >= board.getSize());
	}

	/**
	 * Put the ship on the fields from the board
	 */
	private void placeShipOnBoard(Ship ship, int xPos, int yPos, Orientation orientation) {
		int xDirection = orientation == Orientation.HORIZONTAL ? 1 : 0;
		int yDirection = orientation == Orientation.VERTICAL ? 1 : 0;
		for (int i = 0; i < ship.getSize(); i++) {
			board.getFields()[yPos + i * yDirection][xPos + i * xDirection].setShip(ship);
		}
		ship.place();
	}

	/**
	 * Verifies if the player has set all his ships.
	 *
	 * @return true if all ships have been set, false if not.
	 */
	public boolean hasPlacedAllShips() {
		boolean arePlaced = true;

		for (Ship ship : this.ships) {
			if (!ship.isPlaced()) {
				arePlaced = false;
				break;
			}
		}

		return arePlaced;
	}

	/**
	 * prüft ob der Player das übergebene Schiff tatsächlich besitzt
	 * 
	 * @return gibt true zurück, wenn ja, false wenn nicht.
	 */
	private boolean possessesShip(Ship ship) {
		return Arrays.asList(this.getShips()).contains(ship);
	}

	/**
	 * Setzt das currentShip auf den das nächste Schiff. Die Methode wird beim
	 * Setzen der Schiffe verwendet.
	 */
	public void nextShip() {
		int currentShipIndex = Arrays.asList(this.ships).indexOf(this.currentShip);
		if (currentShipIndex < ships.length - 1) {
			currentShipIndex++;
			currentShip = ships[currentShipIndex];
		}
	}

	/**
	 * Liefert alle Schiffe die nicht zerstört wurden. Zusätzlich können
	 * Schiffe gefiltert werden, die nachladen.
	 * 
	 * @return eine Liste von Schiffen die benutzbar sind
	 */
	public ArrayList<Ship> getAvailableShips(boolean excludeReloadingShips) {
		ArrayList<Ship> availableShips = new ArrayList<Ship>();
		for (Ship ship : ships) {
			if (!ship.isDestroyed()) {
				if (excludeReloadingShips) {
					if (!ship.isReloading()) {
						availableShips.add(ship);
					}
				} else {
					availableShips.add(ship);
				}
			}
		}
		return availableShips;
	}

	/**
	 * Dient zum Setzen des aktuellen Schiffs.
	 */
	public void selectShip(Ship ship) throws Exception {
		if (!possessesShip(ship)) {
			throw new Exception("Player does not possess ship!");
		}
		this.currentShip = ship;
	}

	/**
	 * Markiert das Spieler-Board an der übergebenen Position. Gibt false
	 * zurück, wenn ein Schuss nicht ausgeführt werden kann.
	 * 
	 * @return true, wenn das Board an der übergebenen Position markiert werden
	 *         konnte, false wenn nicht
	 */
	public boolean markBoard(int x, int y) throws FieldOutOfBoardException {
		// Schüsse ignorieren, die außerhalb des Feldes liegen
		if (board.containsFieldAtPosition(x, y)) {
			Field fieldShotAt = board.getField(x, y);
			// wenn Board schon beschossen wurde, dann Schuss ignorieren
			if (!fieldShotAt.isHit()) {
				board.getField(x, y).mark();
				// wenn das Feld auf das geschossen wurde ein Schiff hat,
				// dann ein Leben vom Schiff abziehen
				if (fieldShotAt.hasShip()) {
					Ship ship = fieldShotAt.getShip();
					ship.decreaseSize();
				}
			} else {
				// Feld bereits beschossen
				return false;
			}
			return true;
		}
		return false;
	}

	/**
	 * Check whether all ships are reloading.
	 *
	 * @return true if all ships reload, false if not
	 */
	public boolean areAllShipsReloading() {
		ArrayList<Ship> availableShips = this.getAvailableShips(true);
		return availableShips.size() <= 0;
	}

	public Ship getCurrentShip() {
		return this.currentShip;
	}

	public void setCurrentShip(Ship currentShip) {
		this.currentShip = currentShip;
	}

	

	/**
	 * Returns the number of ships of a certain type.
	 *
	 * @return the number of ships of the given type
	 */
	public int getShipCount(ShipType shipType) {
		int numberOfOccurences = 0;
		for (Ship ship : ships) {
			if (ship.getType() == shipType) {
				if (!ship.isDestroyed() && ship.isPlaced()) {
					numberOfOccurences++;
				}
			}
		}
		return numberOfOccurences;
	}

	/**
	 * Sets the currentShip to the first available ship, whose ship type Is
	 * similar to the given ship type.
	 *
	 * @return the first found ship, the given ship type
	 *
	 */
	public boolean setCurrentShipByType(ShipType shipType) {
		ArrayList<Ship> availableShips = getAvailableShips(true);
		for (Ship ship : availableShips) {
			if (ship.getType() == shipType) {
				currentShip = ship;
				return true;
			}
		}
		return false;
	}

	/**
	 * Indicates whether there are ships that ship to the ShipType Same.
	 *
	 * @return true if ships of the ship type are present, false if not
	 */
	public boolean isShipOfTypeAvailable(ShipType shipType) {
		ArrayList<Ship> availableshShips = getAvailableShips(true);
		for (Ship ship : availableshShips) {
			if (ship.getType() == shipType) {
				return true;
			}
		}
		return false;
	}

	public ShipType getTypeOFirstAvailableShip() {
		ArrayList<Ship> availableshShips = getAvailableShips(true);
		for (Ship ship : availableshShips) {
			return ship.getType();
		}
		return null;
	}

	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		for (Ship ship : ships) {
			ship.setPlaced(true);
		}
		this.board = board;
	}

	public boolean areAllShipsOfTypeReloading(ShipType type) {
		ArrayList<Ship> availableshShips = getAvailableShips(true);
		for (Ship ship : availableshShips) {
			if (ship.getType() == type && !ship.isReloading() && !ship.isDestroyed()) {
				return false;
			}
		}
		return true;
	}

	public void resetShips() {
		for (Ship ship : ships) {
			ship.reset();
		}
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		return this.name;
	}
	public Ship[] getShips() {
		return ships;
	}

	public void setShips(Ship[] ships) {
		this.ships = ships;
	}



	public boolean hasLost() {
		for (Ship ship : ships) {
			if (!ship.isDestroyed()) {
				return false;
			}
		}
		return true;
	}

	public PlayerType getType() {
		return type;
	}

	public void setType(PlayerType type) {
		this.type = type;
	}
}
